package starter.stepdefinitions;

import io.cucumber.java.Before;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.rest.SerenityRest;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.Matchers.*;
import static starter.TestDataManager.titleMatchesProduct;

public class SearchStepDefinitions {

    @Before
    public void before() {
        SerenityRest.given()
                .baseUri("https://waarkoop-server.herokuapp.com")
                .basePath("/api/v1/search/demo/");
    }

    @When("user searches for {string}")
    public void heSearchesFor(String product) {
        SerenityRest.when().get(product);
    }

    @Then("user sees the results displayed for {string}")
    public void heSeesTheResultsFor(String product) {
        restAssuredThat(response -> response
                .body("title", everyItem(titleMatchesProduct(product))));
    }

    @Then("user sees empty result set")
    public void heSeesEmptyResultSet() {
        restAssuredThat(response -> response
                .body("isEmpty()", is(true)));
    }

    @Then("user sees an error message {string}")
    public void heSeesAnErrorMessage(String error) {
        restAssuredThat(response -> response
                .body("detail.error", is(true),"detail.message", is(error)));
    }
}
