package starter;

import org.hamcrest.Matcher;
import org.hamcrest.Matchers;

import java.util.Arrays;
import java.util.stream.Collectors;

public class TestDataManager {
    private static String[] KEY_WORDS_FOR_PASTA = {"Pasta","Penne","Spaghetti","Tagliatelle","Tortellini","Rigate","Rigatoni"};
    private static String[] KEY_WORDS_FOR_APPLE = {"Red Prince", "Gala", "Apple"};
    private static String[] KEY_WORDS_FOR_ORANGE = {"Orange"};
    private static String[] KEY_WORDS_FOR_COLA = {"Pepsi","Coca-Cola"};

    public static Matcher titleMatchesProduct(String product){

        switch (product) {
            case "pasta":
                return containsAnyOfStringIgnoringCase(KEY_WORDS_FOR_PASTA);
            case "apple":
                return containsAnyOfStringIgnoringCase(KEY_WORDS_FOR_APPLE);
            case "orange":
                return containsAnyOfStringIgnoringCase(KEY_WORDS_FOR_ORANGE);
            case "cola":
                return containsAnyOfStringIgnoringCase(KEY_WORDS_FOR_COLA);
            default:
                throw new IllegalArgumentException("Unexpected product: " + product);
        }
    }

    private static Matcher containsAnyOfStringIgnoringCase(String[] values){
        return Matchers.anyOf(Arrays.stream(values).map(value ->
                Matchers.containsStringIgnoringCase(value))
                .collect(Collectors.toList()));
    }
}
