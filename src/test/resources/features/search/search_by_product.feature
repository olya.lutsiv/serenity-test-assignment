Feature: Search for the product

### Please use endpoint GET https://waarkoop-server.herokuapp.com/api/v1/search/demo/{product} for getting the products.
### Available products: "orange", "apple", "pasta", "cola"
### Prepare Positive and negative scenarios

  Scenario: Existing product is available
    When user searches for 'pasta'
    Then user sees the results displayed for 'pasta'

  Scenario: Existing product is not available
    When user searches for 'apple'
    Then user sees empty result set

  Scenario: Product doesn't exist
    When user searches for 'makarony'
    Then user sees an error message 'Not found'