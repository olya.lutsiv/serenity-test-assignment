#Work with the project

### Prerequisites
- Java 1.8 Installation: Ensure Java 1.8 is installed.
- Maven Integration: No need for a separate Maven installation; utilize the provided wrapper (mvnw).

### Building the Project
To successfully build the project and execute tests, use the following command:
```java
mvnw verify
```

### Writing Tests
- Feature Files: Find test cases written in Gherkin within the src\test\resources\features directory.
- Java Implementations: Gherkin test steps are implemented in Java within src\test\java\starter\stepdefinitions.
- Report Generation: The Serenity report is automatically generated post-test execution. You'll find it in target\site\serenity.

--------------------------------------------

# What was refactored and why
- Removed unused dependencies
- Deleted not needed files and folders
- Added maven wrapper
- Fixed tests